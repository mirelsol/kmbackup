#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Backup script in Python that uses rsync capacities to copy files.
Author : Marc SCHNEIDER (marc@mirelsol.org)
"""

import os
import uuid
import argparse
import logging
import sys
import smtplib
import json
from pathlib import Path
from subprocess import run, PIPE
from email.mime.text import MIMEText
from socket import gaierror


logger = None

DEFAULT_NICE_LEVEL = 19 # Lowest priority

backup_tmp_dir = "/tmp/kmbackup/" + str(uuid.uuid4())
backup_tmp_apps_dir = backup_tmp_dir + "/_apps_backup"
default_log_file = backup_tmp_dir + "/backup.log"


def execute_sys_cmd(cmd, dry_run=False, 
                    log_cmd=True, on_error_msg=None, exit_if_error=False, no_error_check=False):
    ret = 0

    if log_cmd:
        logger.info(cmd)
    if not dry_run:
        output = run(cmd, shell=True)
        ret = output.returncode
    if ret == 0 or no_error_check:
        if log_cmd:
            logger.info("Done.")
    else:  # There is an error
        if on_error_msg is not None:
            logger.error(on_error_msg)
        elif log_cmd:
            logger.error("{0}: Command couldn't be executed".format(cmd))
        if exit_if_error:
            sys.exit(1)

    return ret


def is_remote_backup():
    return conf['backup_conf'][media_name]['backup_dest'].get('server') is not None


def get_ssh_string():
    backup_dest = conf['backup_conf'][media_name]['backup_dest']
    ssh_key_path = conf['backup_conf'][media_name]['ssh_key_path']
    if backup_dest['login']:
        return (
            "ssh -i {0} -p {1} -l {2}".format(
                ssh_key_path, backup_dest['port'], backup_dest['login']
            )
        )
    return (
        "ssh -i {0} -p {1}".format(
            ssh_key_path, backup_dest['port']
        )
    )


def backup():
    backup_dest = conf['backup_conf'][media_name]['backup_dest']
    is_remote = is_remote_backup()
    lock_file = backup_dest['dir'] + "/kmbackup.lock"
    ssh_string = ""
    prefix_cmd = ""

    if is_remote:
        ssh_string = get_ssh_string()
        prefix_cmd = "{0} {1} ".format(ssh_string,  backup_dest['server'])

    execute_sys_cmd("{0}test -d '{1}'".format(prefix_cmd, backup_dest['dir']), 
                    on_error_msg="Couldn't access to directory: {0}".format(backup_dest['dir']),
                    exit_if_error=True)

    # Check if a previous backup was interrupted
    ret_lock_file = execute_sys_cmd("{0}test -f '{1}'".format(prefix_cmd, lock_file) , no_error_check=True)
    if ret_lock_file == 0:  # File exists
        logger.error("A previous backup was interrupted, aborting current backup!")
        sys.exit(1)

    # Start backup
    logger.info("Running backup on {0}...".format(media_name))

    # MariaDB / PostgreSQL backups
    if args.mariadb:
        # TODO: what if there is no systemd?
        process_was_running = execute_sys_cmd("systemctl is-active mysqld", args.dry_run, no_error_check=True) == 0
        if not process_was_running:
            execute_sys_cmd("systemctl start mysqld", args.dry_run)
        logger.info("Executing mysqldump...")
        databases = " ".join(conf['mariadb_conf']['databases'])
        mariadb_cmd = "mysqldump -u {0} --password={1} --databases {2} > {3}/mysql.dump".format(
            conf['mariadb_conf']['user'], conf['mariadb_conf']['password'], databases, backup_tmp_apps_dir
        )
        execute_sys_cmd(mariadb_cmd, args.dry_run)
        if not process_was_running:
            execute_sys_cmd("systemctl stop mysqld", args.dry_run)
    if args.postgresql:
        process_was_running = execute_sys_cmd("systemctl is-active postgresql", args.dry_run, no_error_check=True) == 0
        if not process_was_running:
            execute_sys_cmd("systemctl start postgresql", args.dry_run)
        for database in conf['postgresql_conf']['databases']:
            postgresql_cmd = "sudo -u {0} pg_dump -d {1} > {2}/{1}.sql".format(
                conf['postgresql_conf']['shell_user'], database, backup_tmp_apps_dir)
            execute_sys_cmd(postgresql_cmd, args.dry_run)

    if args.system_packages:
        execute_sys_cmd("{0} > {1}/pkglist.txt".format(conf['backup_pkg_cmd'], backup_tmp_apps_dir), args.dry_run)

    # Create a lock file that will be removed at the very end
    execute_sys_cmd("{0}touch '{1}'".format(prefix_cmd, lock_file), args.dry_run, log_cmd=True, exit_if_error=True)

    # Remove oldest backup directory
    dir_to_remove = "{0}/daily.{1}".format(backup_dest['dir'], conf['backup_conf'][media_name]['archive_nb'] - 1)
    exist_dir_cmd = "{0}[ -d {1} ]".format(prefix_cmd, dir_to_remove)
    rm_dir_cmd = "{0}rm -rf '{1}'".format(prefix_cmd, dir_to_remove)

    if execute_sys_cmd(exist_dir_cmd, args.dry_run, log_cmd=False) == 0:
        execute_sys_cmd(rm_dir_cmd, args.dry_run, log_cmd=True,
                        on_error_msg="Couldn't remove {0}".format(dir_to_remove),
                        exit_if_error=True)

    # Shift the snapshots back by one if they exist
    for i in reversed(range(2, conf['backup_conf'][media_name]['archive_nb'])):
        src_dir = "{0}/daily.{1}".format(backup_dest['dir'], i-1)
        dest_dir = "{0}/daily.{1}".format(backup_dest['dir'], i)
        exist_dir_cmd = "{0}[ -d '{1}' ]".format(prefix_cmd, src_dir)
        mv_dir_cmd = "{0}mv '{1}' '{2}'".format(prefix_cmd, src_dir, dest_dir)

        if execute_sys_cmd(exist_dir_cmd, args.dry_run, log_cmd=False) == 0:
            execute_sys_cmd(mv_dir_cmd, args.dry_run, log_cmd=True,
                            on_error_msg="Couldn't move directory {0} to {1}".format(src_dir, dest_dir),
                            exit_if_error=True)

    # Make a hard-link copy (except for directories) of the latest snapshot
    last_backup_dir = "{0}/daily.0".format(backup_dest['dir'])
    dest_backup_dir = "{0}/daily.1".format(backup_dest['dir'])
    copy_cmd = "{0}cp -al '{1}' '{2}'".format(prefix_cmd, last_backup_dir, dest_backup_dir)
    mkdir_cmd = "{0}mkdir '{1}'".format(prefix_cmd, last_backup_dir)
    exist_dir_cmd = "{0}[ -d '{1}' ]".format(prefix_cmd, last_backup_dir)

    if execute_sys_cmd(exist_dir_cmd, args.dry_run, log_cmd=False) == 0:
        # Directory exists
        execute_sys_cmd(copy_cmd, args.dry_run, log_cmd=True,
                        on_error_msg="Couldn't execute {0}".format(copy_cmd),
                        exit_if_error=True)
    else:
        execute_sys_cmd(mkdir_cmd, args.dry_run, log_cmd=True,
                        on_error_msg="Couldn't create directory {0}".format(last_backup_dir),
                        exit_if_error=True)

    # Run rsync from local directories to the latest snapshot (daily.0)
    # rsync behaves like cp --remove-destination by default, so the destination
    # is unlinked first.  If it were not so, this would copy over the other snapshot(s) too!
    #rsync_cmd = "nice --adjust %s rsync -av --progress --relative --delete --delete-excluded %s "\
    #             % (NICE_LEVEL, DIR_TO_BACKUP[media_name])

    base_rsync_cmd = "nice --adjust {0} rsync -avx --progress --relative --delete --delete-excluded".format(args.nice_level)
    target_dir = backup_dest['dir']
    if is_remote:
        base_rsync_cmd += " -e \"{0}\"".format(ssh_string)
        target_dir = "{0}:{1}".format(backup_dest['server'], target_dir)
    dirs = " ".join(conf['backup_conf'][media_name]['dir_to_backup'])
    exclude = "--exclude " + " --exclude ".join(conf['backup_conf'][media_name]['dir_to_exclude'])

    rsync_cmd = "{0} {1} {2} {3}/daily.0".format(base_rsync_cmd, dirs, exclude, target_dir)
    
    execute_sys_cmd(rsync_cmd, args.dry_run, log_cmd=True, exit_if_error=True)

    # rsync local backup (DB backup, packages backup...) to target backup
    base_rsync_cmd = base_rsync_cmd.replace("--relative", "")
    rsync_lb_cmd = "{0} {1} {2}/daily.0".format(base_rsync_cmd, backup_tmp_apps_dir, target_dir)

    execute_sys_cmd(rsync_lb_cmd, args.dry_run, log_cmd=True, exit_if_error=True)

    # Update mtime of daily.0 to reflect the snapshot time
    touch_cmd = "{0}touch '{1}/daily.0'".format(prefix_cmd, backup_dest['dir'])
    
    execute_sys_cmd(touch_cmd, args.dry_run, log_cmd=True, exit_if_error=True)

    # Remove lock file
    execute_sys_cmd("{0}rm -f '{1}'".format(prefix_cmd, lock_file), args.dry_run, log_cmd=True, exit_if_error=True)


def display_disk_usage():
    backup_dest = conf['backup_conf'][media_name]['backup_dest']
    is_remote = is_remote_backup()
    cmd_du = "df -h '{0}'".format(backup_dest['dir'])
    cmd_bck_size = "du '{0}/daily.0' -sh".format(backup_dest['dir'])
    if is_remote:
        ssh_string = get_ssh_string()
        ssh_cmd = "{0} {1}@{2}".format(ssh_string, backup_dest['login'], backup_dest['server'])
        cmd_du = "{0} '{1}'".format(ssh_cmd, cmd_du)
        cmd_bck_size = "{0} {1}".format(ssh_cmd, cmd_bck_size)

    # Backup size
    output = run(cmd_bck_size, shell=True, stdout=PIPE)
    logger.info("Backup size: {0}".format(output.stdout.decode().strip()))
    # Disk usage
    output = run(cmd_du, shell=True, stdout=PIPE)
    logger.info("\n" + output.stdout.decode().strip())


def send_mail():
    if conf.get('mail_conf') is None:
        logger.error("No email configuration found")
        return

    with open(args.log_file) as fp:
        msg = MIMEText(fp.read())

    msg['Subject'] = conf['mail_conf']['mail_subject']
    msg['From'] = conf['mail_conf']['mail_from']
    msg['To'] = conf['mail_conf']['mail_to']

    try:
        try:
            smtp = smtplib.SMTP(host=conf['mail_conf']['smtp_host'], port=conf['mail_conf']['smtp_port'])
        except gaierror as gae:
            logger.error("Error of SMTP configuration")
            logger.error("Check your configuration : {0}".format(conf['mail_conf']))
            logger.error(gae)
            exit(0)
        smtp.login(conf['mail_conf']['smtp_login'], conf['mail_conf']['smtp_password'])
        smtp.send_message(msg)
    except smtplib.SMTPException as se:
        logger.error("Error when trying to send an email")
        logger.error("Check your configuration : {0}".format(conf['mail_conf']))
        logger.error(se)
    finally:
        smtp.quit()


def configure_logger():
    log_format = '%(asctime)s - %(levelname)s - %(message)s'
    log_date_format = '%Y-%m-%d %H:%M:%S'

    # Console logger
    logging.basicConfig(level=logging.INFO, format=log_format, datefmt=log_date_format)
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # File logger
    handler = logging.FileHandler(args.log_file)
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)
    handler.setFormatter(logging.Formatter(fmt=log_format, datefmt=log_date_format))
    
    return logger


if __name__ == "__main__":  
    parser = argparse.ArgumentParser()
    parser.add_argument("conf_file", help="Path to configuration file")
    parser.add_argument("target", help="Target")
    parser.add_argument("--postgresql", help="PostgreSQL backup", action="store_true")
    parser.add_argument("--mariadb", help="MariaDB backup", action="store_true")
    parser.add_argument("-p", "--system-packages", help="Backup of system packages", action="store_true")
    parser.add_argument("-s", "--mail", help="Send email when backup is complete", action="store_true")
    parser.add_argument("-n", "--nice-level", help="Send email when backup is complete", type=int,
                        default=DEFAULT_NICE_LEVEL)
    parser.add_argument("-d", "--dry-run", help="Don't execute the system commands", action="store_true")
    parser.add_argument("-l", "--log-file", help="Log File", type=str, default=default_log_file)

    args = parser.parse_args()

    # Temporary directory for backup
    os.makedirs(backup_tmp_apps_dir)

    logger = configure_logger()
    
    try:
        with open(args.conf_file, "r") as json_conf:
            conf = json.load(json_conf)
    except IOError as err:
        logger.error(err)
        sys.exit(1)

    if args.target not in conf['backup_conf'].keys():
        logger.error("Target must be one of: {0}".format(", ".join(conf['backup_conf'].keys())))
        sys.exit(1)

    media_name = args.target

    backup()

    logger.info("*** Backup is finished!")
    display_disk_usage()

    if args.mail:
        send_mail()
