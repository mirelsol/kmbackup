Backup script in Python that uses rsync capacities to copy files.

# Prerequisites
- GNU/Linux
- rsync
- Python 3

# Features
- Create backups with archives. Size is not doubled each time a new archive is created, because of the use of hardlinks. So when a new archive is created, only the new files are taken in account.
- Possibility to create remote backups over ssh.
- Support of MariaDB / PostgreSQL (experimental).

# Configuration file
The configuration file is rather self-explanatory, adapt it to your needs.
Under the `backup_conf` key, the keys correspond to the targets of your backups.
In the sample file, two are defined: `ext_hdd` and `remote_server`. Feel free to rename them and add new ones.

# Usage

`./km_backup.py <path_to_json_conf_file> <target>`

- Adapt the provided JSON file sample to your needs.
- `<target>` must be one of the keys of `backup_conf` in the JSON configuration file.
- For more options, run: `km_backup.py --help`.

Example: `./kmbackup.py ./kmbackup.json ext_hdd`

**Note**
When starting a backup, kmbackup creates a file named *kmbackup.lock*. If an error occurs during the backup the file won't be deleted. It was made intentionally to prevent the next backups from being inconsistent, i.e. from making new backups until the error has been fixed.
So if you run kmbackup again it will refuse to make the backup as long as there is the file *kmbackup.lock*. You should remove it manually once the problem with the backup has been fixed.


